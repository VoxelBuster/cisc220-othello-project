# CISC220 Othello Project

The game will ask you to input your name upon starting. Enter CPU for one of the names to play against the computer. The game will automatically pick your piece color and randomly pick who goes first.

The game will end when the board is full, or when both players forefeit their turn by placing a piece in a space that will flip no pieces.

I have also added options for the "Harder Computer player extra credit option".

In othello.hpp, there are 2 CPU options. CPU_FORCE_MOVE will force the CPU to select a move that flips pieces where possible when true (Having this false makes the game
very easy as they CPU will often randomly pick a move that flips nothing and therefore forefeit its turn). Setting CPU_SELECT_BEST_MOVE to true will force the CPU to select
the move that flips the most pieces. This will make the game much more difficult.

Good luck!